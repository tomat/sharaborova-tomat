# sharaborova-tomat repository
This git directory was created in order to collaborate in pairs on different projects related to the course **"Scientific Programming for Eginners (SP4E)"**. It also allows access to the teacher and his team to evaluate the work requested.

## Description
As part of the PhD training, we must take a course entitled Scientific Programming for Eginners (SP4E). Several practical works will be delivered during the fall semester 2022.

Our work group is formed by Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) and Mr. Anthony Tomat (anthony.tomat@hesge.ch).

## Homework N°1

Practical work number one (Week 4) is a homework that deal with fews methods to optimize a linear function. One of them is the generalized minimal residual method. It should allow students to become familiar with the Python environment and various libraries to be used in the scientific world.

We have to deliver a work that requires the application of the theoretical elements covered during the first four weeks of the course. The language used is Python and the main libraries to manipulate are scipy, numpy, matplotlib. More information here:
https://gitlab.epfl.ch/anciaux/sp4e/-/blob/master/exercises/week4/GMRES/sujet.pdf
### Prerequisites
The entire development phase was carried out with Python version 3.10.7 in the integrated development environment Visual Studio Code version 1.72.2.

Program is supposed to be run on the same version of Python that the one presented above.

To be able to run well our program you need to download and install others libraries. Please find below the list:

- numpy version 1.23.4
- scipy version 1.9.2
- maplotlib version 3.6.1

To do it, please refer here:
https://packaging.python.org/en/latest/tutorials/installing-packages/


### Exercice n°1: usage of "optimizer.py" 

In order to run the program in good conditions, please open a terminal and place yourself in the folder (cd) where the file "optimizer.py" is located (check with ls command):

```console

$ cd [path-to-the-cloned-folder-where-optimizer.py-is]
$ ls

```

You should see the Python file. Once you are there, please read the help information available to you. To get there:

```console

$ python3 optimizer.py --help

```

The program answers that you can find the values *X = (x,y)* that minimizes the function *S(X)* using various methods using the parameter *-method METHOD*. The available methods are listed on the scipy library web page.

When you don't have a preference for a particular method, you can run the program without parameters and it will use the "BFGS" method by default. For comparison, it will also use the "LGRMES" method.

The results returned are: 
- A graph that represents the evolution of the solution (once saved and/or viewed, please close the viewer window to obtain the following values)
- The values of x and y that minimize the function

More about these functions below:
- BFGS\
https://en.wikipedia.org/wiki/Broyden%E2%80%93Fletcher%E2%80%93Goldfarb%E2%80%93Shanno_algorithm
- LGRMES\
https://en.wikipedia.org/wiki/Generalized_minimal_residual_method

### Exercice n°2: usage of "gmres.py" 
As in the first exercice, place yourself in the folder where the file "*gmres.py*" is (use cd command as before).

In order to launch the program, you have to run this command line :

```console

$ python3 gmres.py method-to-use

```

The method choice must be one of the following :

- customized
- scipy-standard

The first one call our own implementation of the Generalized minimal residual method method whereas the second one ask for the one developped into the scipy library.

There is also others parameters that the user could use. In order to get an overview ask for the help:

```console

$ python3 gmres.py --help

```

**Notice:** that the plotting routine doesn't work actually because we have faced issues with the callback function in our implementation. Due to a lack of time, the definition of the matrix A and the vector b is subject to behaviors that make the program crash. If this is the case, please inform the developers under what conditions the program does not finish correctly.

**IMPORTANT: we mention that the result from our implementation isn't completly correct but at this point we've not enough time to solve this issue.**

## Authors
Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) PhD student

Mr. Anthony Tomat (anthony.tomat@hesge.ch) PhD student
## Project status

- **Homework number 1**: Realized between October 12 and 19, 2022
- **Homework number 2**: to do later
- **Homework number 3**: to do later
- **Homework number 4**: to do later
