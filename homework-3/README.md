# sharaborova-tomat repository
This git directory was created in order to collaborate in pairs on different projects related to the course **"Scientific Programming for Eginners (SP4E)"**. It also allows access to the teacher and his team to evaluate the work requested.

## Description
As part of the PhD training, we must take a course entitled Scientific Programming for Eginners (SP4E). Several practical works will be delivered during the fall semester 2022.

Our work group is formed by Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) and Mr. Anthony Tomat (anthony.tomat@hesge.ch).

## Homework N°3

The goal of this exercise is to use an external library made for scientific computing and adapt it to our needs through a wrapping interface.
We will use the C library FFTW (Fastest Fourier Transform in the West) to code an efficient heat equation solver.
More information here:
https://gitlab.epfl.ch/anciaux/sp4e/-/tree/master/exercises/week11/heat-fft-solver

### How to build 

The simplest way to compile the program:

$ cd heat-fft-solver

$ mkdir build

$ cd build

$ ccmake ..

$ make

The FFTW library by default is ON. If it should be turned off the compilation should be done with:

$ ccmake .. (set FFTW_use OFF)

### Programs usage

test_kepler tests the planet 

test_fft tests Fast Fourier transform and intransform

fft transforms and intransforms matrix, computes the wavenumbers

compute_temperature compute a full step of the time integration


## Authors
Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) PhD student

Mr. Anthony Tomat (anthony.tomat@hesge.ch) PhD student
## Project status

- **Homework number 1**: Realized between October 12 and 19, 2022
- **Homework number 2**: Realized between October 27 and November 02, 2022
- **Homework number 3**: Realized between December 1 and December 15, 2022
- **Homework number 4**: to do later
