#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  int n=m_in.size();
  Matrix<complex> m_out(n);
  fftw_complex *in, *out;
  fftw_plan p;
  in=(fftw_complex*) fftw_malloc(sizeof(fftw_complex)*n*n);
  out=(fftw_complex*) fftw_malloc(sizeof(fftw_complex)*n*n);

  for (auto&& entry:index(m_in)){
    int i=std::get<0>(entry);
    int j=std::get<1>(entry);
    auto& val=std::get<2>(entry);
    in[i*n+j][0]=val.real();
    in[i*n+j][1]=val.imag();
  }
  p=fftw_plan_dft_2d(n,n,in, out, FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(p);

  for(auto&& entry:index(m_out)){
    int i=std::get<0>(entry);
    int j=std::get<1>(entry);
    auto& val=std::get<2>(entry);
    val.real(out[i*n+j][0]);
    val.imag(out[i*n+j][1]);
  }
  fftw_destroy_plan(p);
  fftw_cleanup();
  fftw_free(in);
  fftw_free(out);
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  int n=m_in.size();
  Matrix<complex> m_out(n);
  fftw_complex *in, *out;
  fftw_plan p;
  in=(fftw_complex*) fftw_malloc(sizeof(fftw_complex)*n*n);
  out=(fftw_complex*) fftw_malloc(sizeof(fftw_complex)*n*n);
  for (auto&& entry:index(m_in)){
    int i=std::get<0>(entry);
    int j=std::get<1>(entry);
    auto& val=std::get<2>(entry);
    in[i*n+j][0]=val.real();
    in[i*n+j][1]=val.imag();
  }
  p=fftw_plan_dft_2d(n,n,in,out,FFTW_BACKWARD,FFTW_ESTIMATE);
  fftw_execute(p);
  for (auto&& entry:index(m_out)){
    int i=std::get<0>(entry);
    int j=std::get<1>(entry);
    auto& val=std::get<2>(entry);
    val.real(out[i*n+j][0]/Real(n*n));
    val.imag(out[i*n+j][1]/Real(n*n));
  }
  fftw_destroy_plan(p);
  fftw_cleanup();
  fftw_free(in);
  fftw_free(out);
  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  Matrix<std::complex<int>> results(size);
  int n=size;
  int N=(n-1)/2+1;
  for(auto&& entry: index(results)){
    int i=std::get<0>(entry);
    int j=std::get<1>(entry);
    auto& val=std::get<2>(entry);
    if (i<N)
      val.imag(i);
    else
      val.imag(i-n);
    if (j<N)
      val.real(j);
    else
      val.real(j-n);
  }
  return results;
}

#endif  // FFT_HH
