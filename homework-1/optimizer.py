#   Authors:    Elizaveta Sharaborova [elizaveta.sharaborova@epfl.ch] & Anthony Tomat [anthony.tomat@hesge.ch]
#   Goals:      Use the scipy library to solve an optimization problem and be able to render the result in the 
#               form of a graph using the MatplotLib library

import numpy as np
import matplotlib.pyplot as plt
import argparse
from matplotlib import cm
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres

#   global variables
iterationSolution = []
defmethod = "BFGS"

#   handle parameter
parser = argparse.ArgumentParser()
parser.add_argument("--method", help="Which method to use for the optimization (see possibilities here: 'https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html' by default BFGS is used")
args = parser.parse_args()
if args.method:
    defmethod = args.method

#   add methods from scipy
methods = []
methods.append("Nelder-Mead")
methods.append("Powell")
methods.append("CG")
methods.append("BFGS")
methods.append("Newton-CG")
methods.append("L-BFGS-B")
methods.append("TNC")
methods.append("COBYLA")
methods.append("SLSQP")
methods.append("trust-constr")
methods.append("dogleg")
methods.append("trust-ncg")
methods.append("trust-exact")
methods.append("trust-krylov")

#   test the value of defmethod
if not defmethod in methods:
    print("The programm will be closed. The --method value doesn't match a valid method (see the documentation or ask for help with the instruction: optimizer.py --help)")
    quit()

#   create matrix A and vector b
A = np.array([[8, 1],[1, 3]])
b = np.array([[2],[4]])
TOL = 1.e-10

#   S function to minimize
def S(x):
    #   function value computing
    f = 0.5*((x.T@A)@x)-x.T@b
    return f

def graphic(h1, h2, legend1, legend2):

    #   draw the surface and contours
    figure = plt.figure()
    X, Y = np.mgrid[-3:3:50j, -3:3:50j]
    XY = np.stack([X, Y], axis=0).reshape((2,-1))
    Z = S(XY).diagonal()
    Z = Z.reshape(X.shape)

    ax1 = figure.add_subplot(121, projection="3d")
    ax2 = figure.add_subplot(122, projection="3d")

    ax1.plot_surface(X, Y, Z, cmap=cm.coolwarm, rstride=1, cstride=1, alpha=0.5)
    ax1.contour(X, Y, Z, 10, colors="k", linestyles="solid")
    ax1.set_xlabel('x')
    ax1.set_ylabel('y')
    ax1.set_title(legend1)

    ax2.plot_surface(X, Y, Z, cmap=cm.coolwarm, rstride=1, cstride=1, alpha=0.5)
    ax2.contour(X, Y, Z, 10, colors="k", linestyles="solid")
    ax2.set_xlabel('x')
    ax2.set_ylabel('y')
    ax2.set_title(legend2)

    #   draw solution trajectory
    ax1.plot([item[0] for item in h1],[item[1] for item in h1],[S(item)[0] for item in h1], 'bo', linewidth=1, ls ='--' )
    ax2.plot([item[0] for item in h2],[item[1] for item in h2],[S(item)[0] for item in h2], 'bo', linewidth=1, ls ='--' )
    ax1.set_ylim(3,-3)
    ax1.set_xlim(3,-3)
    ax2.set_ylim(3,-3)
    ax2.set_xlim(3,-3)

    plt.show()

def saving(xk):
    iterationSolution.append(xk)

def optimizing(objective, method="BFGS"):

    x0 = np.array([0.0,0.0])
    iterationSolution.append(x0)

    #   With Method 1: default: BFGS but could be another one (see here:
    #   https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html)
    f = minimize(objective, x0, method=method, tol=TOL, callback=saving)
    historyM1 = iterationSolution.copy()
    solM1 = f.x

    #   reset the iteration history
    iterationSolution.clear()
    iterationSolution.append(x0)

    #   with Method 2: LGMRES
    solM2, status = lgmres(A, b, x0=x0, atol=TOL, callback=saving)
    historyM2 = iterationSolution.copy()

    #   create the matplot object about two methods
    graphic(historyM1, historyM2, method, "GMRES")

    return solM1, solM2

#   compute the result and give back the solution
try:
    s1, s2 = optimizing(S, defmethod)
    print("Result with method ", defmethod, " is [x, y]: ", s1)
    print("Result with method LGMRES is [x, y]: ", s2)
except Exception as e:
    print("An erreur occured: ", str(e))
