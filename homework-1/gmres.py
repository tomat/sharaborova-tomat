#   Authors:    Elizaveta Sharaborova [elizaveta.sharaborova@epfl.ch] & Anthony Tomat [anthony.tomat@hesge.ch]
#   Goals:      Implements a handmade version of the generalized minimal residual method (GMRES)
#
#   Dev.:       Below the declaration to use when you developp and may want to try:
#               Execute all the file and then the next instruction to declare A, b, m, x and threshold
#               A = np.array([[8,1],[1,3]])
#               b = np.array([2,4])
#               m = 10000
#               x = np.zeros(A.shape[0])
#               threshold = 1.e-10
#   Call a res. gmres(A, b, x, m, threshold)
import numpy as np
import argparse
from scipy.sparse.linalg import lgmres

def gmres(A, b, x, m, threshold=1.e-10):

    #   Developped from the Wikipedia example (here: https://en.wikipedia.org/wiki/Generalized_minimal_residual_method)

    shape = A.shape
    n = shape[1]
    #   If m and n are not equals then the algorithm doesn't start
    if not shape[0] == shape[1]:
        raise ValueError('A must be a square matrix n-by-n and it\'s not.')

    #   use x as the initial vector
    r = b - np.einsum('ik,k->i', A, x)

    b_norm = np.linalg.norm(b)
    r_norm = np.linalg.norm(r)

    error = r_norm / b_norm

    #   initialize the 1D vectors
    sn = np.zeros(m)
    cs = np.zeros(m)
    e1 = np.zeros(m+1)
    e1[0] = 1
    e = [error]

    #   initialize Q with n (matrix) and m (max iteration)
    Q = np.zeros((n, m + 1))
    Q[:, 0] = r / r_norm

    #   initialize H with m (matrix)
    H = np.zeros((m + 1, m))
    beta = r_norm * e1

    for k in range(m):

        #   run Arnoldi
        H[0:k+2, k], Q[:,k+1] = arnoldi(A, Q, k)
         
        #   eliminate the last element in H ith row and update the rotation matrix
        H[0:k+2, k], cs[k], sn[k] = applyGivensRotation(H[0:k+2, k], cs, sn, k);

        #   update the residual vector
        beta[k+1] = -sn[k] * beta[k]
        beta[k] = cs[k] * beta[k]
        error = np.abs(beta[k + 1]) / b_norm

        #   save the error
        e.append(error)

        #   if threshold is not reached, k = m at this point (and not m+1) 
        if error <= threshold:
            break

    #   calculate the result
    #   use the least-squares solution and get the first element
    #   => see the documentation here: https://numpy.org/doc/stable/reference/generated/numpy.linalg.lstsq.html)
    y = np.linalg.lstsq(H[0:k+1, 0:k+1], beta[0:k+1], rcond=None)[0]
    #y = y.reshape(y.shape[0],)
    x = x + np.einsum('ik,k->i', Q[:, 0:k+1], y)

    return x, e



def arnoldi(A, Q, k):

    #   Krylov Vector
    q = np.einsum('ik,k->i', A, Q[:,k])
    h = np.zeros(k+2)

    #   Modified Gram-Schmidt, keeping the Hessenberg matrix
    for i in range(k+1):
        h[i] = np.einsum('i,i->',  q, Q[:,i])
        q = q - h[i] * Q[:, i]

    h[k+1] = np.linalg.norm(q)
    q = q / h[k+1]

    return h, q

def applyGivensRotation(h, cs, sn, k):

    #   apply for ith column
    for i in range(k-1):
        temp = cs[i] * h[i] + sn[i] * h[i+1]
        h[i+1] = -sn[i] * h[i] + cs[i] * h[i+1]
        h[i] = temp

    #   update the next sin cos values for rotation
    csK, snK = givensRotation(h[k-1], h[k])

    #   eliminate H(i + 1, i)
    h[k] = csK * h[k] + snK * h[k+1]
    h[k+1] = 0.0

    return h, csK, snK

def givensRotation(v1, v2):
    t = np.sqrt(v1**2 + v2**2)
    cs = v1 / t
    sn = v2 / t
    return cs, sn

#   handle parameters as in this page: https://stackoverflow.com/questions/58636067/how-do-i-use-argparse-to-parse-a-matrix-input
methods = []
methods.append("customized")
methods.append("standard-scipy")
p = argparse.ArgumentParser()
p.add_argument("--plot", help="Whether yes or not a plot need to be delivered (doesn't work actually)")
p.add_argument("method", help="Which method to use for the optimization: 'customized' or 'standard-scipy'")
p.add_argument("--AnRows", action="store", type=int, help="Number of rows for the A matrix")
p.add_argument("--A", action="store", type=int, nargs="+", help="Matrix A of the system")
p.add_argument("--b", action="store", type=int, nargs="+", help="b vector of the system")
args = p.parse_args()

A = np.array([[8,1],[1,3]])
b = np.array([2,4])

if args.AnRows is not None:
    if args.A is not None:
        if args.b is not None:
            A = np.array(args.A).reshape((args.AnRows, len(args.A)//args.AnRows))
            b = np.array(args.b)

if not args.method in methods:
    raise ValueError("Method must be 'customized' or 'standard-scipy'")

#   ask here (below) for the solution
try:
    x = np.zeros(A.shape[0])
    m = 1000
    threshold = 1.e-10
    res = 0
    stat = 0
    if args.method == "customized":
        res, stat = gmres(A, b, x, m, threshold)
    elif args.method == "standard-scipy":
        res, stat = lgmres(A, b, x0=x, atol=threshold)

    print("Result with method " ,args.method, " is [x, y]=", res)

except Exception as e:
    print("An erreur occured: ", str(e))
