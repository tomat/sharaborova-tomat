//
// Created by user on 02.11.22.
//

#ifndef HOMEWORK_2_RIEMANNINTEGRAL_H
#define HOMEWORK_2_RIEMANNINTEGRAL_H


#include <string>
#include <iostream>
#include "Series.h"
class RiemannIntegral {
public:
    RiemannIntegral();

    typedef double(*FUNC)(double);
    RiemannIntegral(double valueA, double valueB, FUNC f);
    virtual ~RiemannIntegral();


    
};


#endif //HOMEWORK_2_RIEMANNINTEGRAL_H
