#include <iostream>
#include <string>
#include <cmath>
#include "Series.h"
#include "ComputeArithmetic.h"
#include "ComputePi.h"
#include <math.h>
#include "RiemannIntegral.h"


int main(int argc, char ** argv) {

    ComputeArithmetic summa;
    double N;
    std::cout<<"Enter the number to count the summ: ";
    std::cin>>N;
    std::cout<<"\n";
    double result=summa.compute(N);
    std::cout<<result<<std::endl<<std::endl;
    ComputePi res;
    std::cout<<"Enter the number approxomation to count the PI: ";
    std::cin>>N;
    std::cout<<"\n";
    result=res.compute(N);
    std::cout<<result<<std::endl;
    double a,b;
    std::cout<<"Result for integral x^3 [0,1]\t";
    RiemannIntegral(0,1,[](double x){return pow(x,3);});
    std::cout<<"Result for integral cos(x) [0,PI]\t";
    RiemannIntegral(0,M_PI,[](double x){return cos(x);});
    std::cout<<"Result for integrsl sin(x) [0,PI/2]\t";
    RiemannIntegral(0,M_PI/2,[](double x){return sin(x);});

    return 0;
}