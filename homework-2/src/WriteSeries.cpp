#include "WriteSeries.hpp"
#include <cmath>
#include <stdio.h>
#include <iomanip>

WriteSeries::WriteSeries(Series & s, int maxiter, int frequency, unsigned int precision, std::string separators) {
            
    this->series = DumperSeries(s);
    this->maxiter = maxiter;
    this->frequency = frequency;
    this->precision = precision;
    this->separators = separators;

}

WriteSeries::~WriteSeries() = default;

void WriteSeries::dump(std::ostream & os) {
    
    double val;
    for (int i = 1; i <= this->maxiter; i += this->frequency) {
        val = this->series.compute(i);
        os << i << this->separators << std::setprecision(this->precision) << val << this->separators << std::endl;
    }
}