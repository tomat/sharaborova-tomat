//
//  PrintSeries.cpp
//  hw2
//
//  Created by Anthony Tomat on 31.10.22.
//

#include "PrintSeries.hpp"
#include <iostream>
#include <cmath>
#include <iomanip>

PrintSeries::PrintSeries(Series &s, int maxiter, int frequency, unsigned int precision){
    
    this->series = DumperSeries(s);
    this->maxiter = maxiter;
    this->frequency = frequency;
    this->precision = precision;
    
}

// Destructor definition
PrintSeries::~PrintSeries() = default;

// Set precision
void PrintSeries::setPrecision(unsigned int precision) {
    this->precision = precision;
}

void PrintSeries::dump(std::ostream &os) {
    
    double init;

    for (int i = 1; i <= this->maxiter; i += this->frequency) {

        init = this->series.compute(i);
        os  << i << "\t" << std::setprecision(this->precision) << init << "\t" << std::endl;
    }
}