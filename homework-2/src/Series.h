//
// Created by user on 02.11.22.
//

#ifndef HOMEWORK_2_SERIES_H
#define HOMEWORK_2_SERIES_H


class Series {
public:
    Series();
    ~Series();
    virtual double compute(unsigned int N)=0;
};


#endif //HOMEWORK_2_SERIES_H
