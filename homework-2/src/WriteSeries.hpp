//
//  PrintSeries.hpp
//  hw2
//
//  Created by Anthony Tomat on 31.10.22.
//

#ifndef WriteSeries_hpp
#define WriteSeries_hpp
#include <stdio.h>
#include "DumperSeries.hpp"

class WriteSeries : public DumperSeries {
    
    private:
    
        // Properties
        int frequency;
        int maxiter;
        unsigned int precision;
        std::string separators;
        
    public:
    
        // Constructor
        WriteSeries(Series & series, int maxiter, int frequency, int precision, string separators);
    
        // Destructor
        virtual ~WriteSeries();

        // Methods
        void dump(std::ostream & os = std::cout) override;
        void setPrecision(unsigned int precision) override;

};

#endif /* WriteSeries_hpp */
