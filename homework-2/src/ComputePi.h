//
// Created by user on 02.11.22.
//

#ifndef HOMEWORK_2_COMPUTEPI_H
#define HOMEWORK_2_COMPUTEPI_H
#include "Series.h"

class ComputePi:public Series {
public:
    ComputePi();
    virtual ~ComputePi();
    double compute(unsigned int N);
};


#endif //HOMEWORK_2_COMPUTEPI_H
