//
// Created by user on 02.11.22.
//

#include "ComputePi.h"
#include <cmath>
ComputePi::ComputePi(){}
ComputePi::~ComputePi(){}

double ComputePi::compute(unsigned int N) {
    double sum=0;
    for (int i=1; i<=N; i++) {
        sum=sum+(1/pow(i,2));
    };
    return sqrt(6*sum);
}