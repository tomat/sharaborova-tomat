//
// Created by user on 02.11.22.
//

#ifndef HOMEWORK_2_COMPUTEARITHMETIC_H
#define HOMEWORK_2_COMPUTEARITHMETIC_H


#include "Series.h"
class ComputeArithmetic: public Series {
public:
    ComputeArithmetic();
    virtual ~ComputeArithmetic();

    double compute(unsigned int N);
};

#endif //HOMEWORK_2_COMPUTEARITHMETIC_H
