//
// Created by user on 02.11.22.
//

#include "ComputeArithmetic.h"
ComputeArithmetic::ComputeArithmetic(){};
ComputeArithmetic::~ComputeArithmetic(){};

double ComputeArithmetic::compute(unsigned int N) {
    double sum=0;
    for (int i=1; i<=N; i++) {
        sum=sum+i;
    };
    return sum;
}