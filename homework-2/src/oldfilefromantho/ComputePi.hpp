//
//  ComputePi.hpp
//  hw2
//
//  Created by Anthony Tomat on 31.10.22.
//

#ifndef ComputePi_hpp
#define ComputePi_hpp

#include <stdio.h>
#include "Series.hpp"

class ComputePi: public Series
{
    public:
    
        // Constructor
        ComputePi();
    
        // Destructor
        virtual ~ComputePi();

        // Methods
        double compute(unsigned int N) override;

};

#endif /* ComputePi_hpp */
