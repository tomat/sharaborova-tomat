//
//  ComputePi.cpp
//  hw2
//
//  Created by Anthony Tomat on 31.10.22.
//

#include "ComputePi.hpp"
#include <cmath>
#include <iostream>
ComputePi::ComputePi(){}
ComputePi::~ComputePi(){}

double ComputePi::compute(unsigned int N){
    
    std::cout << "compute func in Pi" << std::endl;
    double pi = 0.0;
    
    for (int i=1; i==N; i++) {
        
        pi += 1/pow(i, 2);
        
    }
    
    pi = sqrt(6 * pi);
    
    return pi;

}
