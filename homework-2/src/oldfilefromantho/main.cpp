//
//  main.cpp
//  hw2
//
//  Created by Anthony Tomat on 30.10.22.
//

#include <iostream>
#include "Series.hpp"
#include "ComputeArithmetic.hpp"
#include "ComputePi.hpp"
#include "PrintSeries.hpp"

int main(int argc, const char * argv[]) {
    
    ComputePi calculus;
    
    double resultat = calculus.compute(1000);
    
    std::cout << resultat << std::endl;
    
    
    
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}
