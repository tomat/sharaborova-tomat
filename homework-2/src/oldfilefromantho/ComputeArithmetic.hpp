//
//  ComputeArithmetic.hpp
//  hw2
//
//  Created by Anthony Tomat on 30.10.22.
//

#ifndef ComputeArithmetic_hpp
#define ComputeArithmetic_hpp

#include <stdio.h>
#include "Series.hpp"

class ComputeArithmetic: public Series{
    
    public:
    
        // Constructor
        ComputeArithmetic();
    
        // Destructor
        virtual ~ComputeArithmetic();

        // Methods
        double compute(unsigned int N) override;
};

#endif /* ComputeArithmetic_hpp */
