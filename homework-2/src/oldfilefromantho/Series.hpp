//
//  Series.hpp
//  hw2
//
//  Created by Anthony Tomat on 30.10.22.
//

#ifndef Series_hpp
#define Series_hpp

#include <stdio.h>

class Series
{

    public:

        // Constructor
        Series();
    
        // Destructor
        ~Series();

        // Methods
        virtual double compute(unsigned int N) = 0;
};

#endif /* Series_hpp */
