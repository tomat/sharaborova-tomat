//
//  Dumper_Series.hpp
//  hw2
//
//  Created by Anthony Tomat on 30.10.22.
//

#ifndef Dumper_Series_hpp
#define Dumper_Series_hpp

#include <stdio.h>
#include "Series.hpp"
#include <ostream>

class DumperSeries {
    
    protected:

        //  Properties
        Series & series;
    
    public:
    
        //  Constructor
        DumperSeries(Series &series) {
            this->series = series;
        };

        //  Destructor
        virtual ~DumperSeries() {};
    
        //  Methods
        virtual void dump(std::ostream & os) = 0;
        virtual void setPrecision(unsigned int precision) = 0;
    
};

#endif /* Dumper_Series_hpp */

