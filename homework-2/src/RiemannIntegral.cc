//
// Created by user on 02.11.22.
//
#include <string>
#include <iostream>
#include "RiemannIntegral.h"
typedef double(*FUNC)(double);
RiemannIntegral::RiemannIntegral() {}
RiemannIntegral::~RiemannIntegral() {}

double a;
double b;

RiemannIntegral::RiemannIntegral(double valueA, double valueB, FUNC f) {
    a=valueA;
    b=valueB;
    int n=10000000;
    double cumSum = 0;
    double dx=(b-a)/n;
    double lowBound=a;
    for (int i = 0; i < n; i++) {
        double xi = lowBound + i * dx;
        double funValue = f(xi);
        double rectangleArea = funValue * dx;
        cumSum += rectangleArea;
    };
    std::cout<<cumSum<<std::endl<<std::endl;
}