//
//  PrintSeries.hpp
//  hw2
//
//  Created by Anthony Tomat on 31.10.22.
//

#ifndef PrintSeries_hpp
#define PrintSeries_hpp
#include <stdio.h>
#include "DumperSeries.hpp"

class PrintSeries : public DumperSeries {
    
    private:
    
        // Properties
        int frequency;
        int maxiter;
        unsigned int precision;
        
    public:
    
        // Constructor
        PrintSeries(Series & series, int maxiter, int frequency, int precision);
    
        // Destructor
        virtual ~PrintSeries();

        // Methods
        void dump(std::ostream & os = std::cout) override;
        void setPrecision(unsigned int precision) override;

};

#endif /* PrintSeries_hpp */
