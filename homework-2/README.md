# sharaborova-tomat repository Homework 2
This git directory was created in order to collaborate in pairs on project for homework2 related to the course **"Scientific Programming for Eginners (SP4E)"**. It also allows access to the teacher and his team to evaluate the work requested.

## Description
As part of the PhD training, we must take a course entitled Scientific Programming for Eginners (SP4E). Several practical works will be delivered during the fall semester 2022.

Our work group is formed by Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) and Mr. Anthony Tomat (anthony.tomat@hesge.ch).

This file discribes the homework2, what it presents an how to run the program.

## Homework N°2
In this homework we needed to program a family of objects to compute series and to dump them. The program was written in C++ and is represented in different files whis contains classes and methods. The aim of the task is to learn how to use OOP in C++. 
More information about the task can be read in the following link:

https://gitlab.epfl.ch/anciaux/sp4e/-/blob/master/exercises/week6/series/sujet.pdf
### Prerequisites


### How to run the program
To run the program you need to download the whole archive and unzip. Then use any program there you used to run C++ code to compile and run the main.cc file.

## Authors
Ms. Elizaveta Sharaborova (elizaveta.sharaborova@epfl.ch) PhD student

Mr. Anthony Tomat (anthony.tomat@hesge.ch) PhD student

## Comment
After the exercises session on November the 3rd, I was asked to make a comment to the README file, conserning the inheritation of the RiemannIntegral class from the Series, because I couldn't implement it to the task as an inherited class. Could you please give some recomendations about, how it might have been done, so that I still could use it for any type of function (like I realized it in main file with lambda function) and not create functions for each type of task (like: double cubic(), double cos(), double sin()) in RiemannIntegral.cc file?
I would also be gladful for te feedback concerning the implementation of te dumper to the main(), because we written it in separated files but we couldn't understand how to use it properly in the main file.
Thank you!
## Project status

- **Homework number 1**: Realized between October 12 and 19, 2022, link to see:https://gitlab.epfl.ch/tomat/sharaborova-tomat/-/tree/main/homework-1
- **Homework number 2**: Realized between October 27 and November 02, 2022
- **Homework number 3**: to do later
- **Homework number 4**: to do later
